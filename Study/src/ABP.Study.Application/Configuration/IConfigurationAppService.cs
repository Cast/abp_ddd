﻿using System.Threading.Tasks;
using ABP.Study.Configuration.Dto;

namespace ABP.Study.Configuration
{
    public interface IConfigurationAppService
    {
        Task ChangeUiTheme(ChangeUiThemeInput input);
    }
}