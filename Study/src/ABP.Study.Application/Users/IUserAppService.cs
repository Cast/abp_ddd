using System.Threading.Tasks;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ABP.Study.Roles.Dto;
using ABP.Study.Users.Dto;

namespace ABP.Study.Users
{
    public interface IUserAppService : IAsyncCrudAppService<UserDto, long, PagedResultRequestDto, CreateUserDto, UserDto>
    {
        Task<ListResultDto<RoleDto>> GetRoles();
        ListResultDto<UserDto> GetUsers();
    }
}