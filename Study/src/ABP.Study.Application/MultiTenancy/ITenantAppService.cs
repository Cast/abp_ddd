﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using ABP.Study.MultiTenancy.Dto;

namespace ABP.Study.MultiTenancy
{
    public interface ITenantAppService : IAsyncCrudAppService<TenantDto, int, PagedResultRequestDto, CreateTenantDto, TenantDto>
    {
    }
}
