﻿using System;
using System.Collections.Generic;
using System.Text;
using ABP.Study.Tasks.Dtos;
using Abp.Application.Services;
using Abp.Application.Services.Dto;
using System.Threading.Tasks;

namespace ABP.Study.Tasks
{
    public interface ITaskAppService: IApplicationService
    {
        GetTasksOutput GetTasks(GetTasksInput input);

        PagedResultDto<TaskDto> GetPagedTasks(GetTasksInput input);

        void UpdateTask(UpdateTaskInput input);

        int CreateTask(CreateTaskInput input);

        Task<TaskDto> GetTaskByIdAsync(int taskId);

        TaskDto GetTaskById(int taskId);

        void DeleteTask(int taskId);

        //TaskCacheItem GetTaskFromCacheById(int taskId);

        //IList<TaskDto> GetAllTasks();
    }
}
