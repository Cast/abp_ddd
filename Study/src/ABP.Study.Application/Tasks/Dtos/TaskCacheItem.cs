﻿using System;
using System.Collections.Generic;
using System.Text;
using Abp.AutoMapper;

namespace ABP.Study.Tasks.Dtos
{
    [AutoMapFrom(typeof(Task))]
    public class TaskCacheItem
    {
        public string Title { get; set; }
        public string Description { get; set; }
        public TaskState State { get; set; }
    }
}
