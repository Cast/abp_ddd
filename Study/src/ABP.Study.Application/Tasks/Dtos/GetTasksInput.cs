﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABP.Study.Tasks.Dtos
{
    public class GetTasksInput
    {
        public TaskState? State { get; set; }

        public int? AssignedPersonId { get; set; }
    }
}
