﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ABP.Study.Tasks.Dtos
{
    public class GetTasksOutput
    {
        public List<TaskDto> Tasks { get; set; }
    }
}
