﻿using System.Threading.Tasks;
using Abp.Application.Services;
using ABP.Study.Authorization.Accounts.Dto;

namespace ABP.Study.Authorization.Accounts
{
    public interface IAccountAppService : IApplicationService
    {
        Task<IsTenantAvailableOutput> IsTenantAvailable(IsTenantAvailableInput input);

        Task<RegisterOutput> Register(RegisterInput input);
    }
}
