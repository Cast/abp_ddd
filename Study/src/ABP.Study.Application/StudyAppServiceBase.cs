﻿using System;
using System.Threading.Tasks;
using Abp.Application.Services;
using ABP.Study.MultiTenancy;
using Abp.Runtime.Session;
using Abp.IdentityFramework;
using ABP.Study.Authorization.Users;
using Microsoft.AspNetCore.Identity;

namespace ABP.Study
{
    /// <summary>
    /// Derive your application services from this class.
    /// </summary>
    public abstract class StudyAppServiceBase : ApplicationService
    {
        public TenantManager TenantManager { get; set; }

        public UserManager UserManager { get; set; }

        protected StudyAppServiceBase()
        {
            LocalizationSourceName = StudyConsts.LocalizationSourceName;
        }

        protected virtual Task<User> GetCurrentUserAsync()
        {
            var user = UserManager.FindByIdAsync(AbpSession.GetUserId().ToString());
            if (user == null)
            {
                throw new Exception("There is no current user!");
            }

            return user;
        }

        protected virtual Task<Tenant> GetCurrentTenantAsync()
        {
            return TenantManager.GetByIdAsync(AbpSession.GetTenantId());
        }

        protected virtual void CheckErrors(IdentityResult identityResult)
        {
            identityResult.CheckErrors(LocalizationManager);
        }
    }
}