﻿using Abp.Zero.EntityFrameworkCore;
using ABP.Study.Authorization.Roles;
using ABP.Study.Authorization.Users;
using ABP.Study.MultiTenancy;
using ABP.Study.Tasks;
using Microsoft.EntityFrameworkCore;

namespace ABP.Study.EntityFrameworkCore
{
    public class StudyDbContext : AbpZeroDbContext<Tenant, Role, User, StudyDbContext>
    {
        /* Define an IDbSet for each entity of the application */
        public DbSet<Task> Tasks { get; set; }

        public StudyDbContext(DbContextOptions<StudyDbContext> options)
            : base(options)
        {

        }
    }
}
