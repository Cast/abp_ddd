﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ABP.Study.Tasks;

namespace ABP.Study.EntityFrameworkCore.Seed
{
    public class DefaultTestDataForTask
    {
        private readonly StudyDbContext _context;

        private static readonly List<Task> _tasks;

        public DefaultTestDataForTask(StudyDbContext context)
        {
            _context = context;
        }

        static DefaultTestDataForTask()
        {
            _tasks = new List<Task>()
            {
                new Task(1,"Learning ABP deom", "Learning how to use abp framework to build a MPA application."),
                new Task(1,"Make Lunch", "Cook 2 dishs")
            };
        }

        public void Create()
        {
            foreach (var task in _tasks)
            {
                if (_context.Tasks.FirstOrDefault(t => t.Title == task.Title) == null)
                {
                    _context.Tasks.Add(task);
                }
                _context.SaveChanges();
            }
        }
    }
}
