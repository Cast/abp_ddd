﻿using ABP.Study.Configuration;
using ABP.Study.Web;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;

namespace ABP.Study.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class StudyDbContextFactory : IDbContextFactory<StudyDbContext>
    {
        public StudyDbContext Create(DbContextFactoryOptions options)
        {
            var builder = new DbContextOptionsBuilder<StudyDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder());

            StudyDbContextConfigurer.Configure(builder, configuration.GetConnectionString(StudyConsts.ConnectionStringName));
            
            return new StudyDbContext(builder.Options);
        }
    }
}