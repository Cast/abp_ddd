using Microsoft.EntityFrameworkCore;

namespace ABP.Study.EntityFrameworkCore
{
    public static class StudyDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<StudyDbContext> builder, string connectionString)
        {
            //builder.UseSqlServer(connectionString);
            builder.UseMySql(connectionString);
        }
    }
}