﻿using Abp.MultiTenancy;
using ABP.Study.Authorization.Users;

namespace ABP.Study.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {
            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}