﻿using Abp.Authorization;
using ABP.Study.Authorization.Roles;
using ABP.Study.Authorization.Users;

namespace ABP.Study.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
