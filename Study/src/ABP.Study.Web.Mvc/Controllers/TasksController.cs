using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Abp.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using ABP.Study.Users;
using ABP.Study.Tasks;
using ABP.Study.Tasks.Dtos;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace ABP.Study.Web.Mvc.Controllers
{

    [AbpMvcAuthorize]
    public class TasksController : AbpController
    {
        private readonly ITaskAppService _taskAppService;
        private readonly IUserAppService _userAppService;
        public TasksController(ITaskAppService taskAppService, IUserAppService userAppService)
        {
            _taskAppService = taskAppService;
            _userAppService = userAppService;
        }
       

        public PartialViewResult Create()
        {

            var userList = _userAppService.GetUsers();
            ViewBag.AssignedPersonId=new SelectList(userList.Items,"Id","Name");
            return PartialView("_CreateTask");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(CreateTaskInput task)
        {
            var id = _taskAppService.CreateTask(task);
            var input=new GetTasksInput();
            var output = _taskAppService.GetTasks(input);
            return PartialView("_List", output.Tasks);
        }
    }
}