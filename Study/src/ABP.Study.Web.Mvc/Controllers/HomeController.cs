﻿using Abp.AspNetCore.Mvc.Authorization;
using ABP.Study.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace ABP.Study.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : StudyControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}