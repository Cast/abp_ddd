﻿using ABP.Study.Controllers;
using Microsoft.AspNetCore.Mvc;

namespace ABP.Study.Web.Controllers
{
    public class AboutController : StudyControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
	}
}