﻿using System.Reflection;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ABP.Study.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace ABP.Study.Web.Startup
{
    [DependsOn(typeof(StudyWebCoreModule))]
    public class StudyWebMvcModule : AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public StudyWebMvcModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void PreInitialize()
        {
            Configuration.Navigation.Providers.Add<StudyNavigationProvider>();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(StudyWebMvcModule).GetAssembly());
        }
    }
}