﻿using ABP.Study.Configuration.Ui;

namespace ABP.Study.Web.Views.Shared.Components.RightSideBar
{
    public class RightSideBarViewModel
    {
        public UiThemeInfo CurrentTheme { get; set; }
    }
}
