﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace ABP.Study.Web.Views
{
    public abstract class StudyViewComponent : AbpViewComponent
    {
        protected StudyViewComponent()
        {
            LocalizationSourceName = StudyConsts.LocalizationSourceName;
        }
    }
}