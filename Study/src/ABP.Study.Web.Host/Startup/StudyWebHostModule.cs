﻿using System.Reflection;
using Abp.Modules;
using Abp.Reflection.Extensions;
using ABP.Study.Configuration;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;

namespace ABP.Study.Web.Host.Startup
{
    [DependsOn(
       typeof(StudyWebCoreModule))]
    public class StudyWebHostModule: AbpModule
    {
        private readonly IHostingEnvironment _env;
        private readonly IConfigurationRoot _appConfiguration;

        public StudyWebHostModule(IHostingEnvironment env)
        {
            _env = env;
            _appConfiguration = env.GetAppConfiguration();
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(StudyWebHostModule).GetAssembly());
        }
    }
}
